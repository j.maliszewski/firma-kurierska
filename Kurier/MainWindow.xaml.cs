﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database1Entities context = new Database1Entities();
        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Numer_Przesylki.Foreground = Brushes.Black;
            NumerPrzesylki.Focus();
        }

        private void Logowanie_Click(object sender, RoutedEventArgs e)
        {
            PanelLogowania pl = new PanelLogowania();
            this.Close();
            pl.Show();
        }

        private void Szukaj_Click(object sender, RoutedEventArgs e)
        {
            bool Found = false;
            int Id = 0;
            int NumerP = 0;
            bool success = int.TryParse(NumerPrzesylki.Text, out int Parsed);
            if (success)
            {
                foreach (var p in context.Paczkas.ToList())
                {
                    if (p.NumerPrzesylki == int.Parse(NumerPrzesylki.Text))
                    {
                        Id = p.Id;
                        NumerP = p.NumerPrzesylki;
                        Found = true;
                        WyszukanaPrzesylka wp = new WyszukanaPrzesylka(Id, int.Parse(NumerPrzesylki.Text))
                        {
                            Owner = this
                        };
                        wp.ShowDialog();
                    }
                }
                if (!Found)
                {
                    MessageBox.Show("Nie znaleziono przesyłki o numerze: " + NumerPrzesylki.Text, "Nie znaleziono przesyłki", MessageBoxButton.OK, MessageBoxImage.Information);
                } 
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Szukaj_Click(sender, e);
            }
        }
    }
}
