﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurier
{
    public class Paczka_Kurier
    {
        public int Id { get; set; }
        public int NumerPrzesylki { get; set; }
        public string Status { get; set; }
        public string KurierImieNazwisko { get; set; }
    }
}
