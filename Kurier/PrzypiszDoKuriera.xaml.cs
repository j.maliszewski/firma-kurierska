﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Interaction logic for PrzypiszDoKuriera.xaml
    /// </summary>
    public partial class PrzypiszDoKuriera : Window
    {
        Database1Entities context = new Database1Entities();

        private int numerPrzesylki;
        private int id;
        public PrzypiszDoKuriera(int NumerPrzesylki, int Id)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            numerPrzesylki = NumerPrzesylki;
            id = Id;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.numer_przesylki.Content = "Numer przesyłki: " + numerPrzesylki.ToString();
            listbox.ItemsSource = context.KurierModels.ToList();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            KurierModel kurierModel = (KurierModel)listbox.SelectedItem;

            if (kurierModel == null)
            {
                context.Paczkas.Find(id).KurierId = null;
                context.SaveChanges();
                Close();
            }
            else
            {
                context.Paczkas.Find(id).KurierId = kurierModel.Id;
                context.SaveChanges();
                Close();
            }
            
        }
    }
}
