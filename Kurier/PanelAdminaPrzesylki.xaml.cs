﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Interaction logic for PanelAdminaPrzesylki.xaml
    /// </summary>
    public partial class PanelAdminaPrzesylki : Window
    {
        Database1Entities context = new Database1Entities();
        public string NrPrzesylki, Status, Miejscowosc, KodPocztowy, Ulica, NrLokalu, NrTelefonu, Email;
        public static bool typ = false;
        public List<Paczka> paczki = new List<Paczka>();
        public List<Paczka_Kurier> paczka_kurier = new List<Paczka_Kurier>();
        private void SzczegolyPrzesylki(object sender, RoutedEventArgs e)
        {
            Paczka_Kurier paczkakurier = (Paczka_Kurier)ListaPrzesylek.SelectedItem;
            Paczka paczka = context.Paczkas.Find(paczkakurier.Id);
            typ = true;
            FormularzPaczki fp = new FormularzPaczki(typ, paczka.NumerPrzesylki, paczka.Status, paczka.AdresId, paczka.DaneKontaktoweId)
            {
                Owner = this
            };
            fp.Show();
        }
       

        private void RefreshListaPrzesylek()
        {
            Database1Entities context2 = new Database1Entities();
            paczka_kurier.Clear();
            ListaPrzesylek.ItemsSource = "";
            foreach (var paczka in context2.Paczkas)
            {
                if (paczka.Status != "Dostarczona")
                {
                    if (paczka.KurierId != null)
                    {
                        paczka_kurier.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = context2.KurierModels.Find(paczka.KurierId).Imie + " " + context2.KurierModels.Find(paczka.KurierId).Nazwisko
                        });
                    }
                    else
                    {
                        paczka_kurier.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = "Nie przypisano"
                        });
                    }
                }
            }
            ListaPrzesylek.ItemsSource = paczka_kurier.ToList();
        }
        private void UsunPrzesylke(object sender, RoutedEventArgs e)
        {
            if (ListaPrzesylek.SelectedItem != null)
            {
                if (MessageBox.Show("Czy na pewno chcesz usunąć wybraną paczkę?", "Usuń paczkę", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                Paczka_Kurier paczkakurier = (Paczka_Kurier)ListaPrzesylek.SelectedItem;
                Paczka paczka = context.Paczkas.Find(paczkakurier.Id);
                DaneKontaktowe dane = context.DaneKontaktowes.FirstOrDefault(x => x.Id == paczka.DaneKontaktoweId);
                Adre adres = context.Adres.FirstOrDefault(x => x.Id == paczka.AdresId);
                context.Paczkas.Remove(paczka);
                context.DaneKontaktowes.Remove(dane);
                context.Adres.Remove(adres);
                context.SaveChanges();
                RefreshListaPrzesylek();
            }
        }

        private void Szukaj(object sender, TextChangedEventArgs e)
        {
            List<Paczka_Kurier> lista = new List<Paczka_Kurier>();
            foreach (var paczka in paczki)
            {
                if (paczka.NumerPrzesylki.ToString().Contains(Szukaj_txt.Text) ||
                    paczka.Status.ToUpper().Contains(Szukaj_txt.Text.ToUpper()))
                {
                    if(paczka.KurierId != null)
                    {
                        lista.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = context.KurierModels.Find(paczka.KurierId).Imie + " " + context.KurierModels.Find(paczka.KurierId).Nazwisko
                        });
                    }
                    else
                    {
                        lista.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = "Nie przypisano"
                        }); 
                    }
                }
            }

            ListaPrzesylek.ItemsSource = lista.ToList();
        }

        private void Archiwum(object sender, RoutedEventArgs e)
        {
            Archiwum a = new Archiwum();
            this.Close();
            a.Show();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var paczka in context.Paczkas)
            {
                if (paczka.Status != "Dostarczona")
                {
                    paczki.Add(paczka);
                    if (paczka.KurierId != null)
                    {
                        paczka_kurier.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = context.KurierModels.Find(paczka.KurierId).Imie + " " + context.KurierModels.Find(paczka.KurierId).Nazwisko
                        });
                    }
                    else
                    {
                        paczka_kurier.Add(new Paczka_Kurier
                        {
                            Id = paczka.Id,
                            NumerPrzesylki = paczka.NumerPrzesylki,
                            Status = paczka.Status,
                            KurierImieNazwisko = "Nie przypisano"
                        });
                    }
                }
            }
            //ListaPrzesylek.ItemsSource = paczki.ToList();
            ListaPrzesylek.ItemsSource = paczka_kurier.ToList();
            EdytujPrzesylke_button.IsEnabled = false;
            SzczegolyPrzesylki_button.IsEnabled = false;
            UsunPrzesylke_button.IsEnabled = false;
            PrzypiszDoKuriera_button.IsEnabled = false;
        }

        private void ListaPrzesylek_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var actualWidth = ListaPrzesylek.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            if (actualWidth > 0)
            {
                ListView listView = sender as ListView;
                GridView gView = listView.View as GridView;
                var col1 = 0.30;
                var col2 = 0.42;
                var col3 = 0.30;
                gView.Columns[0].Width = actualWidth * col1;
                gView.Columns[1].Width = actualWidth * col2;
                gView.Columns[2].Width = actualWidth * col3;
            }

        }

        private void ListaPrzesylek_Loaded(object sender, RoutedEventArgs e)
        {
            ListView listView = sender as ListView;
            GridView gView = listView.View as GridView;
            var actualWidth = ListaPrzesylek.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            var col1 = 0.30;
            var col2 = 0.42;
            var col3 = 0.30;
            gView.Columns[0].Width = actualWidth * col1;
            gView.Columns[1].Width = actualWidth * col2;
            gView.Columns[2].Width = actualWidth * col3;
        }

        private void DodajPrzesylke(object sender, RoutedEventArgs e)
        {
            FormularzPaczki fp = new FormularzPaczki()
            {
                Owner = this
            };
            fp.ShowDialog();
            RefreshListaPrzesylek();
        }

        private void EdytujPrzesylke(object sender, RoutedEventArgs e)
        {
            Paczka_Kurier paczkakurier = (Paczka_Kurier)ListaPrzesylek.SelectedItem;
            Paczka paczka = context.Paczkas.Find(paczkakurier.Id);
            typ = false;
            FormularzPaczki fp = new FormularzPaczki(typ, paczka.NumerPrzesylki, paczka.Status, paczka.AdresId, paczka.DaneKontaktoweId)
            {
                Owner = this
            };
            fp.ShowDialog();
            if (FormularzPaczki.zmiana)
            {
                paczka.NumerPrzesylki = int.Parse(NrPrzesylki);
                paczka.Status = Status;
                Adre adres = context.Adres.FirstOrDefault(x => x.Id == paczka.AdresId);
                adres.Miejscowosc = Miejscowosc;
                adres.KodPocztowy = KodPocztowy;
                adres.Ulica = Ulica;
                adres.NumerLokalu = NrLokalu;
                DaneKontaktowe dane = context.DaneKontaktowes.FirstOrDefault(x => x.Id == paczka.DaneKontaktoweId);
                dane.NumerTelefonu = NrTelefonu;
                dane.Email = Email;

                if (paczka.Status == "Dostarczona")
                    paczki.Remove(paczka);

                context.SaveChanges();
            }
            RefreshListaPrzesylek();
        }

        public PanelAdminaPrzesylki()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void Wyloguj(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void Kurierzy(object sender, RoutedEventArgs e)
        {
            PanelAdmina pa = new PanelAdmina();
            this.Close();
            pa.Show();
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListaPrzesylek.SelectedItem != null)
            {
                EdytujPrzesylke_button.IsEnabled = true;
                SzczegolyPrzesylki_button.IsEnabled = true;
                UsunPrzesylke_button.IsEnabled = true;
                PrzypiszDoKuriera_button.IsEnabled = true;
            }
            else
            {
                EdytujPrzesylke_button.IsEnabled = false;
                SzczegolyPrzesylki_button.IsEnabled = false;
                UsunPrzesylke_button.IsEnabled = false;
                PrzypiszDoKuriera_button.IsEnabled = false;
            }
        }

        private void PrzypiszDoKuriera_button_Click(object sender, RoutedEventArgs e)
        {
            PrzypiszDoKuriera pdk;
            Paczka_Kurier paczkakurier = (Paczka_Kurier)ListaPrzesylek.SelectedItem;
            Paczka paczkaContext = context.Paczkas.Find(paczkakurier.Id);
            foreach (var przesylka in context.Paczkas.ToList())
            {
                if (paczkaContext == przesylka)
                {
                    pdk = new PrzypiszDoKuriera(przesylka.NumerPrzesylki, przesylka.Id)
                    {
                        Owner = this
                    };
                    pdk.ShowDialog();
                }
            }
            context.SaveChanges();
            RefreshListaPrzesylek();
        }
    }
}
