﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy Archiwum.xaml
    /// </summary>
    public partial class Archiwum : Window
    {
        Database1Entities context = new Database1Entities();
        List<Paczka> paczki = new List<Paczka>();
        public Archiwum()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Przywroc_button.IsEnabled = false;
            Usun_button.IsEnabled = false;
            foreach(var paczka in context.Paczkas)
            {
                if (paczka.Status == "Dostarczona")
                    paczki.Add(paczka);
            }
            ListaPrzesylek.ItemsSource = paczki.ToList();
        }

        private void Wyloguj(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void Przesylki(object sender, RoutedEventArgs e)
        {
            PanelAdminaPrzesylki pap = new PanelAdminaPrzesylki();
            this.Close();
            pap.Show();
        }

        private void Kurierzy(object sender, RoutedEventArgs e)
        {
            PanelAdmina pa = new PanelAdmina();
            this.Close();
            pa.Show();
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ListaPrzesylek.SelectedItem != null)
            {
                Przywroc_button.IsEnabled = true;
                Usun_button.IsEnabled = true;
            }
            else
            {
                Przywroc_button.IsEnabled = false;
                Usun_button.IsEnabled = false;
            }
        }

        private void UsunPrzesylke(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć wybraną paczkę?", "Usuń paczkę", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;
            Paczka p = (Paczka)ListaPrzesylek.SelectedItem;
            Adre adres = context.Adres.FirstOrDefault(x => x.Id == p.AdresId);
            DaneKontaktowe dane = context.DaneKontaktowes.FirstOrDefault(x => x.Id == p.DaneKontaktoweId);
            paczki.Remove(p);
            context.Paczkas.Remove(p);
            context.Adres.Remove(adres);
            context.DaneKontaktowes.Remove(dane);
            context.SaveChanges();
            ListaPrzesylek.ItemsSource = paczki.ToList();

        }

        private void PrzywrocPrzesylke(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz przywrócić wybraną paczkę?", "Usuń paczkę", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;
            Paczka p = (Paczka)ListaPrzesylek.SelectedItem;
            paczki.Remove(p);
            p.Status = "W trakcie realizacji";
            context.SaveChanges();
            ListaPrzesylek.ItemsSource = paczki.ToList();
        }
    }
}
