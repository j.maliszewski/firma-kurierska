﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy PanelKuriera.xaml
    /// </summary>
    public partial class PanelKuriera : Window
    {
        Database1Entities context = new Database1Entities();
        public int Id;
        List<Paczka> paczki = new List<Paczka>();
        public PanelKuriera()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        public PanelKuriera(int id)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            this.Id = id;
            KurierModel kurier = context.KurierModels.FirstOrDefault(x => x.Id == Id);
            Dane_Kuriera.Text = "Witaj, " + kurier.Imie + " " + kurier.Nazwisko;
        }

        private void Wyloguj(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void ZmianaHasla_btn(object sender, RoutedEventArgs e)
        {
            KurierZmianaHasla kzh = new KurierZmianaHasla(Id)
            {
                Owner = this
            };
            kzh.ShowDialog();
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(MojePaczki.SelectedItem != null)
            {
                ZmienStatus_btn.IsEnabled = true;
                Szczegoly_btn.IsEnabled = true;
            }
            else
            {
                ZmienStatus_btn.IsEnabled = false;
                Szczegoly_btn.IsEnabled = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ZmienStatus_btn.IsEnabled = false;
            Szczegoly_btn.IsEnabled = false;

                foreach (var paczka in context.Paczkas)
                {
                    if (paczka.KurierId == this.Id)
                        paczki.Add(paczka);
                }
                MojePaczki.ItemsSource = paczki.ToList();

        }

        private void ZmienStatus(object sender, RoutedEventArgs e)
        {
            Database1Entities context2 = new Database1Entities();
            Paczka paczka = (Paczka)MojePaczki.SelectedItem;
            ZmianaStatusuPrzesylki zsp = new ZmianaStatusuPrzesylki(paczka.Id)
            {
                Owner = this
            };
            zsp.ShowDialog();
            paczki.Clear();
            foreach (var pcz in context2.Paczkas)
            {
                if (pcz.KurierId == this.Id)
                    paczki.Add(pcz);
            }

            MojePaczki.ItemsSource = paczki.ToList();
        }

        private void Szczegoly(object sender, RoutedEventArgs e)
        {
            Paczka paczka = (Paczka)MojePaczki.SelectedItem;
            FormularzPaczki fp = new FormularzPaczki(true, paczka.NumerPrzesylki, paczka.Status, paczka.AdresId, paczka.DaneKontaktoweId)
            {
                Owner = this
            };
            fp.Show();
        }

        private void Szukaj(object sender, TextChangedEventArgs e)
        {
            List<Paczka> lista = new List<Paczka>();
            foreach (var paczka in paczki)
            {
                if (paczka.NumerPrzesylki.ToString().Contains(Szukaj_txt.Text) || paczka.Status.ToUpper().Contains(Szukaj_txt.Text.ToUpper()))
                    lista.Add(paczka);
            }
            MojePaczki.ItemsSource = lista.ToList();

        }

        private void MojePaczki_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var actualWidth = MojePaczki.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            if (actualWidth > 0)
            {
                ListView listView = sender as ListView;
                GridView gView = listView.View as GridView;
                var col1 = 0.49;
                var col2 = 0.50;
                gView.Columns[0].Width = actualWidth * col1;
                gView.Columns[1].Width = actualWidth * col2; 
            }

        }

        private void MojePaczki_Loaded(object sender, RoutedEventArgs e)
        {
            ListView listView = sender as ListView;
            GridView gView = listView.View as GridView;
            var actualWidth = MojePaczki.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            var col1 = 0.49;
            var col2 = 0.50;
            gView.Columns[0].Width = actualWidth * col1;
            gView.Columns[1].Width = actualWidth * col2;
        }
    }
}
