﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy PanelLogowania.xaml
    /// </summary>
    public partial class PanelLogowania : Window
    {
        Database1Entities context = new Database1Entities();
        List<Konto> konta = new List<Konto>();

        public PanelLogowania()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            konta = context.Kontoes.ToList();
            NazwaUzytkownika.Focus();
        }

        private void StronaGlowna_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void Zaloguj_button_Click(object sender, RoutedEventArgs e)
        {
            //byte[] salt;
            //new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            //var pbkdf2 = new Rfc2898DeriveBytes(Haslo.Password, salt, 10000);
            //byte[] hash = pbkdf2.GetBytes(20);

            //byte[] hashBytes = new byte[36];
            //Array.Copy(salt, 0, hashBytes, 0, 16);
            //Array.Copy(hash, 0, hashBytes, 16, 20);
            //string PasswordHash = Convert.ToBase64String(hashBytes);
            bool czyPoprawnieZalogowano = true;
            int ileKont = 0;

            foreach (Konto k in konta)
            {
                if (NazwaUzytkownika.Text == k.Login)
                {
                    /* Fetch the stored value */
                    string savedPasswordHash = k.Haslo;
                    /* Extract the bytes */
                    byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
                    /* Get the salt */
                    byte[] salt = new byte[16];
                    Array.Copy(hashBytes, 0, salt, 0, 16);
                    /* Compute the hash on the password the user entered */
                    var pbkdf2 = new Rfc2898DeriveBytes(Haslo.Password, salt, 10000);
                    byte[] hash = pbkdf2.GetBytes(20);
                    /* Compare the results */
                    for (int i = 0; i < 20; i++)
                    {
                        if (hashBytes[i + 16] != hash[i])
                        {
                            MessageBox.Show("Błędny login lub hasło!", "Błąd logowania", MessageBoxButton.OK, MessageBoxImage.Information);
                            czyPoprawnieZalogowano = false;
                            NazwaUzytkownika.Clear();
                            Haslo.Clear();
                            NazwaUzytkownika.Focus();
                            break;
                        }
                    }

                    if (k.TypKonta == "kurier" && czyPoprawnieZalogowano)
                    {
                        PanelKuriera pk = new PanelKuriera(k.Id);
                        this.Close();
                        pk.Show();
                        break;
                    }
                    if (k.TypKonta == "admin" && czyPoprawnieZalogowano)
                    {
                        PanelAdmina pa = new PanelAdmina();
                        this.Close();
                        pa.Show();
                        break;
                    }
                }
                ileKont++;
                if (ileKont >= konta.Count() && czyPoprawnieZalogowano)
                {
                    MessageBox.Show("Błędny login lub hasło!", "Błąd logowania", MessageBoxButton.OK, MessageBoxImage.Information);
                    NazwaUzytkownika.Clear();
                    Haslo.Clear();
                    NazwaUzytkownika.Focus();
                }
            }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Zaloguj_button_Click(sender, e);
            }
        }
    }
}
