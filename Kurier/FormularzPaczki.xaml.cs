﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy FormularzPaczki.xaml
    /// </summary>
    
    public partial class FormularzPaczki : Window
    {
        Database1Entities context = new Database1Entities();
        bool czyWylosowano = false;
        int nr;
        public static bool zmiana = false;
        static Regex RegKodPocztowy = RegexKodPocztowy();
        private static Regex RegexKodPocztowy()
        {
            string Pattern = @"^\d{2}-\d{3}$";

            return new Regex(Pattern, RegexOptions.IgnoreCase);
        }
        static Regex RegNrTelefonu = RegexNrTelefonu();
        private static Regex RegexNrTelefonu()
        {
            string Pattern = @"^\d{9}$";

            return new Regex(Pattern, RegexOptions.IgnoreCase);
        }

        public FormularzPaczki()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Random random = new Random();
            while (!czyWylosowano)
            {
                nr = random.Next(100000, 999999);
                if (context.Paczkas.Count() != 0)
                {
                    foreach (Paczka p in context.Paczkas)
                    {
                        if (p.NumerPrzesylki == nr)
                        {
                            czyWylosowano = false;
                            break;
                        }
                        else
                            czyWylosowano = true;
                    }
                }
                else
                    czyWylosowano = true;

            }

            NrPrzesylki.Text = nr.ToString();
            NrPrzesylki.IsEnabled = false;
        }

        public FormularzPaczki(bool typ, int _numer, string _status, int _adresId, int? _daneId)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            if (!typ)
                this.Title = "Edycja";
            else
            {
                this.Title = "Szczegóły";
                NrPrzesylki.IsEnabled = false;
                Status.IsEnabled = false;
                AdresGrouBox.IsEnabled = false;
                DaneGroupBox.IsEnabled = false;
                ZapiszButton.Content = "Zamknij";
            }

            NrPrzesylki.Text = _numer.ToString();
            Status.SelectedValue = _status;
            Adre adres = context.Adres.FirstOrDefault(x => x.Id == _adresId);
            Miejscowosc.Text = adres.Miejscowosc;
            KodPocztowy.Text = adres.KodPocztowy;
            Ulica.Text = adres.Ulica;
            NumerLokalu.Text = adres.NumerLokalu;
            DaneKontaktowe dane = context.DaneKontaktowes.FirstOrDefault(x => x.Id == _daneId);
            NumerTelefonu.Text = dane.NumerTelefonu;
            Email.Text = dane.Email;
        }

        private void DodajPaczke(object sender, RoutedEventArgs e)
        {

            if (this.Title == "Edycja")
            {
                if (czyPoprawnyEmail(Email.Text) && czyPoprawnyKod(KodPocztowy.Text) && czyPoprawnyNrTelefonu(NumerTelefonu.Text))
                {
                    PanelAdminaPrzesylki mainW = Application.Current.Windows.OfType<PanelAdminaPrzesylki>().FirstOrDefault();
                    mainW.NrPrzesylki = NrPrzesylki.Text;
                    mainW.Status = Status.Text;
                    mainW.Miejscowosc = Miejscowosc.Text;
                    mainW.KodPocztowy = KodPocztowy.Text;
                    mainW.Ulica = Ulica.Text;
                    mainW.NrLokalu = NumerLokalu.Text;
                    mainW.NrTelefonu = NumerTelefonu.Text;
                    mainW.Email = Email.Text;
                    zmiana = true;
                    Close();
                }
                else
                {
                    MessageBox.Show("Błędny kod pocztowy, nr telefonu, bądź adres e-mail!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if(this.Title == "Szczegóły")
            {
                Close();
            }
            else
            {
                if (NrPrzesylki.Text != "" && Status.Text != "" && Miejscowosc.Text != "" && KodPocztowy.Text != "" && Ulica.Text != "" && NumerLokalu.Text != "" && NumerTelefonu.Text != "" && Email.Text != "")
                {
                    if (czyPoprawnyEmail(Email.Text) && czyPoprawnyKod(KodPocztowy.Text) && czyPoprawnyNrTelefonu(NumerTelefonu.Text))
                    {
                        DaneKontaktowe dane = new DaneKontaktowe
                        {
                            NumerTelefonu = NumerTelefonu.Text,
                            Email = Email.Text
                        };
                        context.DaneKontaktowes.Add(dane);

                        Adre adres = new Adre
                        {
                            Miejscowosc = Miejscowosc.Text,
                            KodPocztowy = KodPocztowy.Text,
                            Ulica = Ulica.Text,
                            NumerLokalu = NumerLokalu.Text
                        };
                        context.Adres.Add(adres);
                        context.SaveChanges();

                        Paczka paczka = new Paczka
                        {
                            NumerPrzesylki = int.Parse(NrPrzesylki.Text),
                            Status = Status.Text,
                            AdresId = adres.Id,
                            DaneKontaktoweId = dane.Id
                        };
                        context.Paczkas.Add(paczka);
                        context.SaveChanges();

                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Błędny kod pocztowy, nr telefonu, bądź adres e-mail!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Pola nie mogą być puste!", "Uzupełnij dane", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            
        }
        bool czyPoprawnyEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        internal static bool czyPoprawnyKod(string kod)
        {
            bool isValid = RegKodPocztowy.IsMatch(kod);

            return isValid;
        }
        internal static bool czyPoprawnyNrTelefonu(string nr)
        {
            bool isValid = RegNrTelefonu.IsMatch(nr);

            return isValid;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> statusy = new List<string>();
            statusy.Add("Dodana do bazy");
            statusy.Add("W trakcie realizacji");
            statusy.Add("Wydana dla kuriera");
            statusy.Add("Dostarczona");
            Status.ItemsSource = statusy.ToList();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
           // zmiana = false;
            Close();
        }
    }
}
