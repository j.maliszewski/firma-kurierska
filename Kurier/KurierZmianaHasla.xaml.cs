﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy KurierZmianaHasla.xaml
    /// </summary>
    public partial class KurierZmianaHasla : Window
    {
        Database1Entities context = new Database1Entities();
        int Id;
        bool czyPoprawneDane = true;

        public KurierZmianaHasla(int _id)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Id = _id;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Konto konto = context.Kontoes.FirstOrDefault(x => x.Id == Id);

            /* Fetch the stored value */
            string savedPasswordHash = konto.Haslo;
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(stareHaslo.Password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                {
                    MessageBox.Show("Błędne hasło!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    czyPoprawneDane = false;
                    stareHaslo.Clear();
                    noweHaslo.Clear();
                    noweHasloPowtorz.Clear();
                    stareHaslo.Focus();
                    break;
                }
            }

            if (noweHaslo.Password.Equals(noweHasloPowtorz.Password) && czyPoprawneDane)
            {
                byte[] salt2;
                new RNGCryptoServiceProvider().GetBytes(salt2 = new byte[16]);

                var pbkdf2_2 = new Rfc2898DeriveBytes(noweHaslo.Password, salt2, 10000);
                byte[] hash_2 = pbkdf2_2.GetBytes(20);

                byte[] hashBytes_2 = new byte[36];
                Array.Copy(salt2, 0, hashBytes_2, 0, 16);
                Array.Copy(hash_2, 0, hashBytes_2, 16, 20);
                string PasswordHash = Convert.ToBase64String(hashBytes_2);

                konto.Haslo = PasswordHash;
                context.SaveChanges();

                MessageBox.Show("Hasło zmieniono poprawnie!", "Sukces", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else if(czyPoprawneDane)
            {
                MessageBox.Show("Hasła muszą być identyczne!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                noweHaslo.Clear();
                noweHasloPowtorz.Clear();
                noweHaslo.Focus();
            }
            czyPoprawneDane = true;
        }
    }
}
