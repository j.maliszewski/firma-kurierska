﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy WyszukanaPrzesylka.xaml
    /// </summary>
    public partial class WyszukanaPrzesylka : Window
    {
        Database1Entities context = new Database1Entities();
        private int NumerPrzesylki;
        private int Id;
        public WyszukanaPrzesylka(int _Id, int _NumerPrzesylki)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            NumerPrzesylki = _NumerPrzesylki;
            Id = _Id;
            InitializeComponent();
        }

        private void Zamknij_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainW = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            mainW.NumerPrzesylki.Focus();
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Paczka paczka;            
            paczka = context.Paczkas.Find(Id);
            NumerPrzesylki_textbox.Text = paczka.NumerPrzesylki.ToString();
            StatusPrzesylki_textbox.Text = paczka.Status.ToString();
        }
    }
}
