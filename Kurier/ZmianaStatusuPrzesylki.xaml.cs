﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Interaction logic for ZmianaStatusuPrzesylki.xaml
    /// </summary>
    public partial class ZmianaStatusuPrzesylki : Window
    {
        Database1Entities context = new Database1Entities();
        private int Id;
        public ZmianaStatusuPrzesylki(int _Id)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Id = _Id;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Paczka paczka = context.Paczkas.Find(Id);
            List<string> statusy = new List<string>();
            statusy.Add("Dodana do bazy");
            statusy.Add("W trakcie realizacji");
            statusy.Add("Wydana dla kuriera");
            statusy.Add("Dostarczona");
            Status.ItemsSource = statusy.ToList();
            NumerPrzesylki.Content = "Numer przesyłki:" + paczka.NumerPrzesylki.ToString();
            Status.SelectedValue = paczka.Status;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            context.Paczkas.Find(Id).Status = Status.Text;
            context.SaveChanges();
            this.Close();
        }
    }
}
