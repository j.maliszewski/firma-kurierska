﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Interaction logic for PanelAdmina.xaml
    /// </summary>
    public partial class PanelAdmina : Window
    {
        Database1Entities context = new Database1Entities();
        public string Imie, Nazwisko, NumerTelefonu, Pesel;
        public static bool typOkna = false;
        public PanelAdmina()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            ListaKurierow.ItemsSource = context.KurierModels.ToList();
            EdytujKuriera_btn.IsEnabled = false;
            UsunKuriera_btn.IsEnabled = false;
            SzczegolyKuriera_btn.IsEnabled = false;
            ResetujHaslo_btn.IsEnabled = false;
        }

        private void DodajKuriera_button(object sender, RoutedEventArgs e)
        {
            FormularzKurier fk = new FormularzKurier()
            {
                Owner = this
            };
            fk.ShowDialog();
            ListaKurierow.ItemsSource = context.KurierModels.ToList();
        }

        private void Wyloguj(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void Przesylki(object sender, RoutedEventArgs e)
        {
            PanelAdminaPrzesylki pap = new PanelAdminaPrzesylki();
            this.Close();
            pap.Show();
        }

        private void Szukaj(object sender, TextChangedEventArgs e)
        {
            List<KurierModel> lista = new List<KurierModel>();
            foreach(var kurier in context.KurierModels)
            {
                if (kurier.Imie.ToUpper().Contains(Szukaj_txt.Text.ToUpper()) || kurier.Nazwisko.ToUpper().Contains(Szukaj_txt.Text.ToUpper())) 
                    lista.Add(kurier);
            }

            ListaKurierow.ItemsSource = lista.ToList();
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListaKurierow.SelectedItem != null)
            {
                EdytujKuriera_btn.IsEnabled = true;
                UsunKuriera_btn.IsEnabled = true;
                SzczegolyKuriera_btn.IsEnabled = true;
                ResetujHaslo_btn.IsEnabled = true;
            }
            else
            {
                EdytujKuriera_btn.IsEnabled = false;
                UsunKuriera_btn.IsEnabled = false;
                SzczegolyKuriera_btn.IsEnabled = false;
                ResetujHaslo_btn.IsEnabled = false;
            }
        }

        private void Archiwum(object sender, RoutedEventArgs e)
        {
            Archiwum a = new Archiwum();
            this.Close();
            a.Show();
        }

        private void UsunKuriera_button(object sender, RoutedEventArgs e)
        {
            if (ListaKurierow.SelectedItem != null)
            {
                if (MessageBox.Show("Czy na pewno chcesz usunąć wskazanego kuriera?", "Usuń kuriera", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                KurierModel kurier = (KurierModel)ListaKurierow.SelectedItem;
                foreach(var paczka in kurier.Paczkas)
                {
                    paczka.KurierId = null;
                }
                Konto konto = context.Kontoes.FirstOrDefault(x => x.Id == kurier.Id);
                context.KurierModels.Remove(kurier);
                context.Kontoes.Remove(konto);
                context.SaveChanges();
                ListaKurierow.ItemsSource = context.KurierModels.ToList();
            }
        }

        private void EdytujKuriera_button(object sender, RoutedEventArgs e)
        {
            KurierModel kurier = (KurierModel)ListaKurierow.SelectedItem;
            FormularzKurier fk = new FormularzKurier("Edycja", kurier.Imie, kurier.Nazwisko, kurier.NumerTelefonu, kurier.Pesel)
            {
                Owner = this
            };
            fk.ShowDialog();

            if (FormularzKurier.zmiana)
            {
                kurier.Imie = Imie;
                kurier.Nazwisko = Nazwisko;
                kurier.NumerTelefonu = NumerTelefonu;
                kurier.Pesel = Pesel;
                context.SaveChanges();

                Konto konto = context.Kontoes.FirstOrDefault(x => x.Id == kurier.Id);
                konto.Login = kurier.Pesel;
                context.SaveChanges();
            }

            ListaKurierow.ItemsSource = context.KurierModels.ToList();

        }

        private void SzczegolyKuriera_button(object sender, RoutedEventArgs e)
        {
            KurierModel kurier = (KurierModel)ListaKurierow.SelectedItem;
            FormularzKurier fk = new FormularzKurier("Szczegoly", kurier.Imie, kurier.Nazwisko, kurier.NumerTelefonu, kurier.Pesel)
            {
                Owner = this
            };
            fk.ShowDialog();
        }

        private void ResetujHaslo_button(object sender, RoutedEventArgs e)
        {
            if (ListaKurierow.SelectedItem != null)
            {
                if (MessageBox.Show("Czy na pewno chcesz zresetować hasło dla wybranego kuriera?", "Reset hasła", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                KurierModel kurier = (KurierModel)ListaKurierow.SelectedItem;
                Konto konto = context.Kontoes.FirstOrDefault(x => x.Id == kurier.Id);

                string haslo = kurier.Imie + "." + kurier.Nazwisko;

                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

                var pbkdf2 = new Rfc2898DeriveBytes(haslo, salt, 10000);
                byte[] hash = pbkdf2.GetBytes(20);

                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);
                string PasswordHash = Convert.ToBase64String(hashBytes);

                konto.Haslo = PasswordHash;
                context.SaveChanges();
                ListaKurierow.ItemsSource = context.KurierModels.ToList();

                MessageBox.Show("Hasło zresetowano pomyślnie!", "Reset hasła", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
