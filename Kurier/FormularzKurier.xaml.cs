﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kurier
{
    /// <summary>
    /// Logika interakcji dla klasy FormularzKurier.xaml
    /// </summary>
    public partial class FormularzKurier : Window
    {
        Database1Entities context = new Database1Entities();
        static Regex RegPesel = RegexPesel();
        public static bool zmiana = false;

        private static Regex RegexPesel()
        {
            string Pattern = @"^\d{11}$";

            return new Regex(Pattern, RegexOptions.IgnoreCase);
        }
        static Regex RegNrTelefonu = RegexNrTelefonu();
        private static Regex RegexNrTelefonu()
        {
            string Pattern = @"^\d{9}$";

            return new Regex(Pattern, RegexOptions.IgnoreCase);
        }
        public FormularzKurier()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        public FormularzKurier(string typ, string _Imie, string _Nazwisko, string _NumerTelefonu, string _Pesel)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            this.Title = typ;
            Imie.Text = _Imie;
            Nazwisko.Text = _Nazwisko;
            NumerTelefonu.Text = _NumerTelefonu;
            Pesel.Text = _Pesel;
            if (this.Title == "Szczegoly")
            {
                Imie.IsEnabled = false;
                Nazwisko.IsEnabled = false;
                NumerTelefonu.IsEnabled = false;
                Pesel.IsEnabled = false;
                DodajButton.Content = "Zamknij";
            }
        }

        private void Dodaj_button(object sender, RoutedEventArgs e)
        {
            bool bledneDane = false;

            if (this.Title == "Edycja")
            {
                if (czyPoprawnyNrTelefonu(NumerTelefonu.Text) && czyPoprawnyPesel(Pesel.Text))
                {
                    PanelAdmina mainW = Application.Current.Windows.OfType<PanelAdmina>().FirstOrDefault();
                    mainW.Imie = Imie.Text;
                    mainW.Nazwisko = Nazwisko.Text;
                    mainW.NumerTelefonu = NumerTelefonu.Text;
                    mainW.Pesel = Pesel.Text;
                    zmiana = true;
                    Close();
                }
                else
                {
                    MessageBox.Show("Błędny nr telefonu, bądź PESEL!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (this.Title == "Szczegoly")
            {
                Close();
            }
            else
            {
                if (Imie.Text != "" && Nazwisko.Text != "" && NumerTelefonu.Text != "" && Pesel.Text != "")
                {
                    if (czyPoprawnyNrTelefonu(NumerTelefonu.Text) && czyPoprawnyPesel(Pesel.Text))
                    {
                        foreach (KurierModel k in context.KurierModels)
                        {
                            if (k.Pesel == Pesel.Text)
                            {
                                MessageBox.Show("Osoba o tym peselu już istnieje.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                                bledneDane = true;
                                Pesel.Focus();
                                break;
                            }
                        }

                        if (!bledneDane)
                        {
                            KurierModel kurier = new KurierModel
                            {
                                Imie = Imie.Text,
                                Nazwisko = Nazwisko.Text,
                                Pesel = Pesel.Text,
                                NumerTelefonu = NumerTelefonu.Text
                            };

                            context.KurierModels.Add(kurier);
                            context.SaveChanges();
                            int id = 0;

                            foreach (KurierModel k in context.KurierModels)
                            {
                                if (kurier.Imie == k.Imie && kurier.Nazwisko == k.Nazwisko && kurier.NumerTelefonu == k.NumerTelefonu && kurier.Pesel == k.Pesel)
                                {
                                    id = k.Id;
                                    break;
                                }
                            }

                            string haslo = Imie.Text + "." + Nazwisko.Text;

                            byte[] salt;
                            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

                            var pbkdf2 = new Rfc2898DeriveBytes(haslo, salt, 10000);
                            byte[] hash = pbkdf2.GetBytes(20);

                            byte[] hashBytes = new byte[36];
                            Array.Copy(salt, 0, hashBytes, 0, 16);
                            Array.Copy(hash, 0, hashBytes, 16, 20);
                            string PasswordHash = Convert.ToBase64String(hashBytes);

                            Konto konto = new Konto
                            {
                                Id = id,
                                Login = kurier.Pesel,
                                Haslo = PasswordHash,
                                TypKonta = "kurier"
                            };
                            context.Kontoes.Add(konto);
                            context.SaveChanges();
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Błędny nr telefonu, bądź PESEL!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Pola nie mogą być puste!", "Uzupełnij dane", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

        }

        internal static bool czyPoprawnyPesel(string kod)
        {
            bool isValid = RegPesel.IsMatch(kod);

            return isValid;
        }
        internal static bool czyPoprawnyNrTelefonu(string nr)
        {
            bool isValid = RegNrTelefonu.IsMatch(nr);

            return isValid;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Close();
        }

    }
}
