Temat 4. Kurier, zespół: Adam Solon, Jakub Maliszewski, Mariusz Grynczel

Administrator:
- Dodawanie nowych przesyłek
- Edycja istniejących przesyłek
- Zmiana statusu przesyłki
- Usuwanie i archiwizowanie przesyłek
- Wyświetlanie listy wszystkich paczek i szczegółów
- Dodawanie kurierów do bazy kurierów
- Przypisywanie kurierów do paczek
- CRUD kurierzy

Kurier:
- Zmiana statusu przesyłki
- Wyświetlanie przesyłek przypisanych do niego

Zwykły użytkownik:
- Po nr przesyłki może wyświetlić szczegóły

Pierwszy panel po otwarciu aplikacji będzie służył do wpisania numeru przesyłki dla
niezalogowanego użytkownika. Będzie dostępny poniżej przycisk „Zaloguj”, który
przeniesie do panelu logowania dla administratorów i kurierów.

Administrator w panelu administracyjnym będzie miał możliwość dodawania nowych
przesyłek za pomocą formularza, edycji za pomocą podobnego formularza, co w
dodawaniu, ale wypełniony obecnymi danymi. W podglądzie przesyłek będzie mógł
kliknąć w status danej przesyłki i np. z listy będzie wybierał status tej przesyłki. W
panelu wszystkich przesyłek będą również odpowiednie przyciski „usuń” oraz
„zarchiwizuj”. Po zarchiwizowaniu paczki, przenosi się ona do innego panelu, gdzie są
dostępne tylko przesyłki w archiwum. W oddzielnym panelu, administrator będzie
miał dostęp do zarządzania bazą kurierów. Będzie tam formularz dodawania i edycji,
podobnie jak w przesyłkach oraz możliwość usuwania kurierów i przypisania ich do
poszczególnych przesyłek w podglądzie wszystkich przesyłek.

Kurier w swoim panelu będzie mógł zmieniać status w przypisanych do niego
przesyłkach oraz w tym samym panelu będą widoczne wszystkie przypisane do niego
paczki.
Zwykły użytkownik, który nie będzie miał konta w aplikacji, będzie mógł na
oddzielnej stronie znaleźć swoją przesyłkę za pomocą numeru przesyłki i sprawdzić jej
status oraz wybrane szczegóły.